import React from 'react';
import { Route } from 'react-router';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import Home from './containers/Home';

const App = () => {
	return (
		<Router>
			<Switch>
				<Route path='/' exact component={Home} />
			</Switch>
		</Router>
	);
};

export default App;
