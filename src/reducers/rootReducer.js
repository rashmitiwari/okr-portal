import { combineReducers } from 'redux';

const mainReducer = combineReducers({});

const rootReducer = (state, action) => {
	return mainReducer(state, action);
};

export default rootReducer;
