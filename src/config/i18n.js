import i18n from 'i18next';
import English from './locales/en-US.json';

const resources = {
	'en-US': {
		translation: English,
	},
};

i18n.init({
	lng: 'en-US',
	fallbackLng: 'en-US',
	nsSeparator: false,
	keySeparator: false,
	// TODO: Break this out into a provider
	// debug: process.env.NODE_ENV !== 'production',
	interpolation: {
		escapeValue: false,
	},
	resources,
});

export default i18n;
